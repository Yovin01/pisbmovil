import 'dart:math';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/physics.dart';
import 'package:pisbmovil/models/Sensores.dart';
import 'package:pisbmovil/responsive.dart';
import 'package:pisbmovil/screens/dashboard/components/my_fields.dart';
import 'package:flutter/material.dart';
import 'package:pisbmovil/services/conexion.dart';

import '../../constants.dart';
import 'components/header.dart';

import 'components/recent_files.dart';
import 'components/recomendacion_details.dart';

class DashboardScreen extends StatelessWidget {
  conexion _conn = new conexion();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        primary: false,
        padding: EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            Header(),
            Align(
              alignment: Alignment.centerRight, // Alineación a la izquierda
              child: Switch(
                  value:
                      AdaptiveTheme.of(context).mode == AdaptiveThemeMode.light,
                  activeThumbImage:
                      new AssetImage('assets/images/sun-svgrepo-com.png'),
                  inactiveThumbImage: new AssetImage(
                      'assets/images/moon-stars-svgrepo-com.png'),
                  activeColor: Colors.white,
                  activeTrackColor: Colors.amber,
                  inactiveThumbColor: Colors.black,
                  inactiveTrackColor: Colors.white,
                  onChanged: (bool value) {
                    if (value) {
                      AdaptiveTheme.of(context).setLight();
                    } else {
                      AdaptiveTheme.of(context).setDark();
                    }
                  }),
            ),
            SizedBox(height: defaultPadding),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      //MyFiles(),
                      StorageDetails(),
                      SizedBox(height: defaultPadding),
                      //RecentFiles(),
                      if (Responsive.isMobile(context))
                        SizedBox(height: defaultPadding),
                      if (Responsive.isMobile(context)) MyFiles(),
                    ],
                  ),
                ),
                if (!Responsive.isMobile(context))
                  SizedBox(width: defaultPadding),
                // On Mobile means if the screen is less than 850 we don't want to show it
                if (!Responsive.isMobile(context))
                  Expanded(
                    flex: 2,
                    child: StorageDetails(),
                  ),
              ],
            )
          ],
        ),
      ),
    );
  }

  respose() async{
    String token = "NO_TOKEN";
    var response = await _conn.solicitudGet('/medicionDispositivos', token);
    return response.toString();
  }
}
