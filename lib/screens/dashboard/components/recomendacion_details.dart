import 'package:flutter/material.dart';
import 'package:pisbmovil/services/facade/ListaSensoresFacade.dart';
import 'package:pisbmovil/services/facade/modelo/ListaSensores.dart';
import 'package:pisbmovil/services/facade/modelo/Sensores.dart';

import '../../../constants.dart';
import 'chart.dart';
import 'recomendaciones_info_card.dart';

class StorageDetails extends StatelessWidget {
  const StorageDetails({
    Key? key,
  }) : super(key: key);

  Future<List<Sensores>> _listar() async {
    ListaSensoresFacade laf = ListaSensoresFacade();
    ListaSensores lista = await laf.listaSensor();
    return lista.data;
  }
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Sensores>>(
      future: _listar(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          double summedidas = 0.0;
          for (int i = 0; i < snapshot.data!.length; i++) {
            double medida = double.parse(snapshot.data![i].medicion.toStringAsFixed(2));
            summedidas += medida;
          }
          measure = double.parse((summedidas/snapshot.data!.length).toStringAsFixed(2));
          return Container(
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Hola, 👋 \nExplora datos de radiación UV",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: defaultPadding),
                Chart(),
                Text(
                  "Recomendaciones",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                cards()
              ],
            ),
          );
        }
      },
    );
  }

  cards(){
    if (measure >= 0.0 && measure < 3.0) {
      return Column(
          children:[RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-summer-svgrepo-com.svg",
        title: "Necesita protección mínima",
        subtitle: "Riesgo: Bajo",
      ),RecomendacionesInfoCard(
    svgSrc: "assets/icons/sunglasses-svgrepo-com.svg",
    title: "Use gafas con filtro UV",
    subtitle: "Riesgo: Bajo",
    )]);
    } else if (measure >= 3.0 && measure < 6.0) {
      return Column(
          children:[RecomendacionesInfoCard(
        svgSrc: "assets/icons/cap-svgrepo-com.svg",
        title: "Use gorra o sombrero",
        subtitle: "Riesgo: Moderado",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sunglasses-svgrepo-com.svg",
        title: "Use gafas con filtro UV",
        subtitle: "Riesgo: Moderado",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-protection-svgrepo-com.svg",
        title: "Utilice crema con filtro solar",
        subtitle: "Riesgo: Moderado",
      )]);
    } else if (measure >= 6.0 && measure < 8.0) {
      return Column(
          children:[RecomendacionesInfoCard(
        svgSrc: "assets/icons/cap-svgrepo-com.svg",
        title: "Use gorra o sombrero",
        subtitle: "Riesgo: Alto",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sunglasses-svgrepo-com.svg",
        title: "Use gafas con filtro UV",
        subtitle: "Riesgo: Alto",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-protection-svgrepo-com.svg",
        title: "Utilice crema con filtro solar",
        subtitle: "Riesgo: Alto",
      )]);
    } else if (measure >= 8.0 && measure < 11.0) {
      return Column(
          children:[RecomendacionesInfoCard(
        svgSrc: "assets/icons/cap-svgrepo-com.svg",
        title: "Use gorra o sombrero",
        subtitle: "Riesgo: Muy Alto",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sunglasses-svgrepo-com.svg",
        title: "Use gafas con filtro UV",
        subtitle: "Riesgo: Muy Alto",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-protection-svgrepo-com.svg",
        title: "Utilice crema con filtro solar",
        subtitle: "Riesgo: Muy Alto",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-2-svgrepo-com.svg",
        title: "Procure no exponerse al sol",
        subtitle: "Riesgo: Muy Alto",
      )]);
    } else if (measure >= 11.0 && measure <= 15.0) {
      return Column(
          children:[RecomendacionesInfoCard(
        svgSrc: "assets/icons/house-person-svgrepo-com.svg",
        title: "Evite la exposición al sol",
        subtitle: "Riesgo: Extremo",
      ),RecomendacionesInfoCard(
        svgSrc: "assets/icons/sun-protection-svgrepo-com.svg",
        title: "Utilice crema con filtro solar alto SPF 30+",
        subtitle: "Riesgo: Extremo",
      )]);
    } else {
      return Column(
    children: [Text("Lo sentimos presentamos algunos problemas...")],
    );
    }
  }

}
