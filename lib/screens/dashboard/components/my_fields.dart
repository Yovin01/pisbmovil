import 'dart:developer';

import 'package:pisbmovil/models/Sensores.dart';
import 'package:pisbmovil/responsive.dart';
import 'package:flutter/material.dart';
import 'package:pisbmovil/screens/dashboard/components/chart.dart';
import 'package:pisbmovil/screens/dashboard/components/info_card.dart';
import 'package:pisbmovil/services/facade/ListaSensoresFacade.dart';
import 'package:pisbmovil/services/facade/modelo/ListaSensores.dart';
import 'package:pisbmovil/services/facade/modelo/Sensores.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../../constants.dart';
import 'file_info_card.dart';

class MyFiles extends StatelessWidget {
  const MyFiles({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Datos Sensores",
              style: Theme.of(context).textTheme.titleMedium,
            ),
            /**ElevatedButton.icon(
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(
                  horizontal: defaultPadding * 1.5,
                  vertical:
                      defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
                ),
              ),
              onPressed: () {},
              icon: Icon(Icons.add),
              label: Text("Add New"),
            ),*/
          ],
        ),
        SizedBox(height: defaultPadding),
        Responsive(
          mobile: FileInfoCardGridView(
            crossAxisCount: _size.width < 650 ? 2 : 4,
            childAspectRatio: _size.width < 650 && _size.width > 350 ? 1.3 : 1,
          ),
          tablet: FileInfoCardGridView(),
          desktop: FileInfoCardGridView(
            childAspectRatio: _size.width < 1400 ? 1.1 : 1.4,
          ),
        ),
      ],
    );
  }
}

class FileInfoCardGridView extends StatelessWidget {
  const FileInfoCardGridView({
    Key? key,
    this.crossAxisCount = 4,
    this.childAspectRatio = 1,
  }) : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;

  Future<List<Sensores>> _listar() async {
    ListaSensoresFacade laf = ListaSensoresFacade();
    ListaSensores lista = await laf.listaSensor();
    return lista.data;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Sensores>>(
      future: _listar(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        log(snapshot.toString());
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          String getNameFromId(int id) {
            switch (id) {
              case 1:
                return 'Motupe';
              case 2:
                return 'Punzara';
              case 3:
                return 'FEIRNNR';
              case 4:
                return 'Veterinaria';
              case 5:
                return 'Contabilidad';
              case 6:
                return 'Medicina';
              default:
                return 'Desconocido';
            }
          }

          List demoMyFiles = [];

          for (int i = 0; i < snapshot.data!.length; i++) {
            double medida =
                double.parse(snapshot.data![i].medicion.toStringAsFixed(2));
            int id = snapshot.data![i].id;
            demoMyFiles.add(
              CloudStorageInfo(
                title: getNameFromId(id), //snapshot.data![i].nombre
                numOfFiles: medida,
                svgSrc: "assets/icons/sun-svgrepo-com.svg",
                totalStorage: deftitle(medida),
                color: Color(defColor(medida)),
                percentage: (medida * 100) / 15,
              ),
            );
          }
          return GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: demoMyFiles.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: crossAxisCount,
              crossAxisSpacing: defaultPadding,
              mainAxisSpacing: defaultPadding,
              childAspectRatio: childAspectRatio,
            ),
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                // Realiza la acción que desees al presionar el elemento
                // Por ejemplo, aquí puedes mostrar un diálogo con información del archivo
                /*Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LineChartSample2(),
            ),
          );*/
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('${snapshot.data![index].nombre}'),
                    content: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () {
                              switch (snapshot.data![index].id) {
                                case 1:
                                  launchUrlString(
                                      'https://www.google.com/maps/place/3%C2%B056\'43.2%22S+79%C2%B013\'22.8%22W/@-3.9453437,-79.2228585,19.33z/data=!4m4!3m3!8m2!3d-3.9453346!4d-79.2230074?entry=ttu');
                                case 2:
                                  launchUrlString(
                                      "https://www.google.com/maps/place/4%C2%B002\'25.1%22S+79%C2%B012\'35.9%22W/@-4.040303,-79.209969,17z/data=!3m1!4b1!4m4!3m3!8m2!3d-4.040303!4d-79.209969?entry=ttu");
                                case 3:
                                  launchUrlString(
                                      'https://www.google.com/maps/place/4%C2%B001\'55.1%22S+79%C2%B011\'59.0%22W/@-4.0317892,-79.1993886,18.87z/data=!4m4!3m3!8m2!3d-4.0319671!4d-79.1997164?entry=ttu');
                                case 4:
                                  launchUrlString(
                                      'https://www.google.com/maps/place/4%C2%B002\'11.3%22S+79%C2%B012\'10.4%22W/@-4.036461,-79.202875,17z/data=!3m1!4b1!4m4!3m3!8m2!3d-4.036461!4d-79.202875?entry=ttu');
                                case 5:
                                  launchUrlString(
                                      'https://www.google.com/maps/place/Carrera+de+Contabilidad+y+Auditor%C3%ADa/@-4.0371869,-79.2068318,17z/data=!3m1!4b1!4m6!3m5!1s0x91cb37983e6ae22f:0x553cbb706a84523b!8m2!3d-4.0371869!4d-79.2042569!16s%2Fg%2F11syqp_l9l?entry=ttu');
                                case 6:
                                  launchUrlString(
                                      'https://www.google.com/maps/place/3%C2%B059\'34.6%22S+79%C2%B012\'26.5%22W/@-3.9928061,-79.2072253,19.33z/data=!4m4!3m3!8m2!3d-3.9929348!4d-79.2073715?entry=ttu');
                                default:
                                  const SnackBar mensaje = SnackBar(
                                      content: Text(
                                          'Lo sentimos no se ha encontrado lo que esta buscando'));
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(mensaje);
                              }
                            },
                            child: InfoCard(
                              svgSrc:
                                  "assets/icons/location-information-svgrepo-com.svg",
                              title: "Ubicación  del dispositivo",
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context); // Cerrar el diálogo
                        },
                        child: Text('Cerrar'),
                      ),
                    ],
                  ),
                );
              },
              child: FileInfoCard(info: demoMyFiles[index]),
            ),
          );
        }
      },
    );
  }
}
