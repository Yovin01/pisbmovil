import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:pisbmovil/services/facade/ListaSensoresFacade.dart';
import 'package:pisbmovil/services/facade/modelo/ListaSensores.dart';
import 'package:pisbmovil/services/facade/modelo/Sensores.dart';

import '../../../constants.dart';


class Chart extends StatefulWidget {
  const Chart({super.key});

  @override
  State<StatefulWidget> createState() => ChartState();
}

Future<List<Sensores>> _listar() async {
  ListaSensoresFacade laf = ListaSensoresFacade();
  ListaSensores lista = await laf.listaSensor();
  return lista.data;
}

class ChartState extends State {
  int touchedIndex = -1;
  @override
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Sensores>>(
      future: _listar(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          double sumMedidas = 0.0;
          for (int i = 0; i < snapshot.data!.length; i++) {
            double medida = double.parse(snapshot.data![i].medicion.toStringAsFixed(2));
            sumMedidas += medida;
          }
          measure = double.parse((sumMedidas/snapshot.data!.length).toStringAsFixed(2));
          if(measure.isNaN){
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Por favor revise su conexión'),
                  Image.asset('assets/images/cat_sin_net.png'), // Reemplaza 'imagen_conexion.png' con la ruta de tu imagen
                  SizedBox(height: 10), // Espacio entre la imagen y el texto
                ],
              ),
            );
          }
          return SizedBox(
            height: 200,
            child: Stack(
              children: [
                PieChart(
                  PieChartData(
                    pieTouchData: PieTouchData(
                      touchCallback: (FlTouchEvent event, pieTouchResponse) {
                        setState(() {
                          if (!event.isInterestedForInteractions ||
                              pieTouchResponse == null ||
                              pieTouchResponse.touchedSection == null) {
                            touchedIndex = -1;
                            return;
                          }
                          touchedIndex = pieTouchResponse
                              .touchedSection!.touchedSectionIndex;
                        });
                      },
                    ),
                    sectionsSpace: 0,
                    centerSpaceRadius: 70,
                    startDegreeOffset: -90,
                    sections: showingSections(),
                  ),
                  swapAnimationDuration: Duration(milliseconds: 200), // Optional
                  swapAnimationCurve: Curves.linear,
                ),
                Positioned.fill(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: defaultPadding),
                      Text(
                        measure.toString(),
                        style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                          color: Theme.of(context).cardColor,
                          fontWeight: FontWeight.w600,
                          height: 0.5,
                        ),
                      ),
                      Text("UV")
                    ],
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }


  List<PieChartSectionData> showingSections() {
    return List.generate(2, (i) {
      final isTouched = i == touchedIndex;
      //final fontSize = isTouched ? 25.0 : 16.0;
      final radius = isTouched ? 25.0 : 13.0;
      final mostrar = isTouched ? true : false;
      //measure = constants().measure();
      double value(){
        return (measure*100)/15;
      }
      /**
       * int color(){
          if (measure >= 0 && measure <= 2) {
          return 0xFF00FF00; // Verde
          } else if (measure >= 3.0 && measure <= 5.0) {
          return 0xFFFFFF00; // Amarillo
          } else if (measure >= 6.0 && measure <= 7.0) {
          return 0xFFFFA500; // Anaranjado
          } else if (measure >= 8.0 && measure <= 10.0) {
          return 0xFFFF0000; // Rojo
          } else if (measure >= 11.0 && measure <= 15.0) {
          return 0xFF8A2BE2; // Violeta
          }
          return 0x00000000;
          }
       * */
      int color = 0x00000000;
      String title = "";
      if (measure >= 0.0 && measure < 3) {
        title = "Bajo";
        color = 0xFF00FF00; // Verde
      } else if (measure >= 3.0 && measure < 6.0) {
        title = "Moderado";
        color = 0xFFFFFF00; // Amarillo
      } else if (measure >= 6.0 && measure < 8.0) {
        title = "Alto";
        color =  0xFFFFA500; // Anaranjado
      } else if (measure >= 8.0 && measure < 11.0) {
        title = "Muy Alto";
        color = 0xFFFF0000; // Rojo
      } else if (measure >= 11.0 && measure <= 15.0) {
        title = "Extremo";
        color =  0xFF8A2BE2; // Violeta
      }
      const shadows = [Shadow(color: Colors.black, blurRadius: 2)];
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Color(color),
            value: value(),
            title: title,
            showTitle: mostrar,
            radius: radius,
            titleStyle: TextStyle(
              //fontSize: fontSize,
              fontWeight: FontWeight.bold,
              color: Colors.white,
              shadows: shadows,
            ),
          );
        case 1:
          return PieChartSectionData(
            color: primaryColor.withOpacity(0.1),
            value: (100 - value()),
            //title: '30%',
            showTitle: false,
            radius: 13,
            /**titleStyle: TextStyle(
              fontSize: fontSize,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrange,
              shadows: shadows,
            ),**/
          );
        default:
          throw Error();
      }
    });
  }
}