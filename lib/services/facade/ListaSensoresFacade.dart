import 'dart:convert';

import 'package:pisbmovil/services/conexion.dart';
import 'package:pisbmovil/services/facade/modelo/ListaSensores.dart';
import 'package:pisbmovil/services/utiles/Utilidades.dart';

class ListaSensoresFacade{
  conexion _conn = new conexion();
  Utilidades _util = new Utilidades();
  Future<ListaSensores> listaSensor() async{
    var response = await _conn.solicitudGet('/medicionDispositivos', "NO");
    return _response((response.code != 0)? response.info: null);
  }
}

ListaSensores _response(dynamic data){
  var sesion = ListaSensores();
  if(data!=null){
    Map<String, dynamic> mapa = jsonDecode(data);
    if(mapa.containsKey("ultimasMediciones")){
      List datos = jsonDecode(jsonEncode(mapa["ultimasMediciones"]));
      sesion = ListaSensores.fromMap(datos, int.parse(mapa["code"].toString()));
    }else{
      List myList = List.empty();
      sesion = ListaSensores.fromMap(myList, int.parse(mapa["code"].toString()));
    }
  }
  return sesion;
}