

import 'dart:convert';

class Sensores {
  String nombre = '';
  String external_id = '';
  String latitud = '';
  String longitud = '';
  int id = 0;
  double medicion = 0.0;
  Sensores();

  Sensores.fromMap(Map<dynamic, dynamic> mapa){
    nombre = mapa ['nombre'];
    external_id = mapa ['external_id'];
    latitud = mapa ['latitud'].toString();
    longitud = mapa ['longitud'].toString();
    id = mapa ['id'];
    medicion = double.parse(mapa ['medicions'][0]['uv'].toString());
  }

  @override
  String toString() {
    return 'Nombre: $nombre, External ID: $external_id, Latitud: $latitud, Longitud: $longitud, ID: $id, Medicion: $medicion';
  }

  static Map<String, dynamic> toMap(Sensores model) =>
      <String, dynamic> {
        "nombre": model.nombre,
        "external_id": model.external_id,
        "latitud": model.latitud,
        "longitud": model.longitud,
        "id": model.id,
        "medicion": model.medicion
      };

  static String serialize(Sensores model) =>
      json.encode(Sensores.toMap(model));
}