import 'package:pisbmovil/services/facade/modelo/Sensores.dart';
import 'package:pisbmovil/services/modelo/RespuestaGenerica.dart';

class ListaSensores extends RespuestaGenerica{
  late List<Sensores> data = [];
  ListaSensores();
  ListaSensores.fromMap(List datos, int code){
    datos.forEach((item) {
      Map<dynamic, dynamic> mapa = item;
      Sensores aux = Sensores.fromMap(mapa);
      data.add(aux);
    });
    this.msg = msg;
    this.code = code;
  }
}