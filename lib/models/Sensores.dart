import 'dart:ffi';

import 'package:pisbmovil/constants.dart';
import 'package:flutter/material.dart';
import 'package:pisbmovil/services/facade/ListaSensoresFacade.dart';
import 'package:pisbmovil/services/facade/modelo/ListaSensores.dart';
import 'package:pisbmovil/services/facade/modelo/Sensores.dart';
import 'package:pisbmovil/services/utiles/Utilidades.dart';

class CloudStorageInfo{
  final String? svgSrc, title, totalStorage;

  //final int?  percentage;
  final double? numOfFiles, percentage;
  final Color? color;
  double _medida = 0.0;

  CloudStorageInfo({
    this.svgSrc,
    this.title,
    this.totalStorage,
    this.numOfFiles,
    this.percentage,
    this.color,
  });

  getMedida (){
    medida();
    return 0.0;
  }
  medida() async{
    double valor = await _tomarmedida();
    return _medida;
  }

  Future<double> _tomarmedida() async {
    Utilidades util = new Utilidades();
    ListaSensoresFacade laf = ListaSensoresFacade();
    ListaSensores lista = await laf.listaSensor();
    _medida = lista.data[0].medicion as double;
    print("prueba: ${_medida}");
    return _medida;
  }
}

  List demoMyFiles = [
    /*CloudStorageInfo(
      title: "FEIRNNR",
      numOfFiles:7.0,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(7.0),
      color: Color(defColor(7.0)),
      percentage: (7.0 * 100) / 15,
    ),
    CloudStorageInfo(
      title: "Contabilidad",
      numOfFiles: 1.2,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(1.2),
      color: Color(defColor(1.2)),
      percentage: (1.2 * 100) / 15,
    ),
    CloudStorageInfo(
      title: "Medicina",
      numOfFiles: 7.5,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(7.5),
      color: Color(defColor(7.5)),
      percentage: (7.5 * 100) / 15,
    ),
    CloudStorageInfo(
      title: "Punzara",
      numOfFiles: 7.0,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(7.0),
      color: Color(defColor(7.0)),
      percentage: (7.0 * 100) / 15,
    ),
    CloudStorageInfo(
      title: "Motupe",
      numOfFiles: 15.0,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(15.0),
      color: Color(defColor(15.0)),
      percentage: (15.0 * 100) / 15,
    ),
    CloudStorageInfo(
      title: "Veterinaria",
      numOfFiles: 3.4,
      svgSrc: "assets/icons/sun-svgrepo-com.svg",
      totalStorage: deftitle(3.4),
      color: Color(defColor(3.4)),
      percentage: (3.4 * 100) / 15,
    ),*/
  ];
/**
 * colores:
 * Color(0xFFFFA113)
 * Color(0xFFA4CDFF)
 * Color(0xFF007EE5)
 * */

defColor (double measure){
  if (measure >= 0.0 && measure < 3) {
    return 0xFF00FF00; // Verde
  } else if (measure >= 3.0 && measure < 6.0) {
    return 0xFFFFFF00; // Amarillo
  } else if (measure >= 6.0 && measure < 8.0) {
    return 0xFFFFA500; // Anaranjado
  } else if (measure >= 8.0 && measure < 11.0) {
    return 0xFFFF0000; // Rojo
  } else if (measure >= 11.0 && measure <= 15.0) {
    return 0xFF8A2BE2; // Violeta
  }
}
deftitle (double measure){
  if (measure >= 0.0 && measure < 3) {
    return "Bajo"; // Verde
  } else if (measure >= 3.0 && measure < 6.0) {
    return "Moderado"; // Amarillo
  } else if (measure >= 6.0 && measure < 8.0) {
    return "Alto"; // Anaranjado
  } else if (measure >= 8.0 && measure < 11.0) {
    return "Muy Alto"; // Rojo
  } else if (measure >= 11.0 && measure <= 15.0) {
    return "Extremo"; // Violeta
  }
}


