import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:pisbmovil/constants.dart';
import 'package:pisbmovil/controllers/MenuAppController.dart';
import 'package:pisbmovil/screens/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
        dark: ThemeData.dark().copyWith(
          primaryColor: Color(0xFF2A2D3E),
          cardColor: Colors.white70,
          scaffoldBackgroundColor: Color(0xFF212332),
          textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
              .apply(bodyColor: Colors.white),
          canvasColor: secondaryColor,
        ),
        light: ThemeData.light().copyWith(
          primaryColor: Color(0xB3FFFFFF),
          //primaryColor: Color(0xFF0B243A),
          cardColor: Colors.black,
          scaffoldBackgroundColor: Color(0xFFBEE3F8),
          //scaffoldBackgroundColor: Colors.white,
          textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
              .apply(bodyColor: Colors.black),
          canvasColor: Colors.black54,
        ),
        initial: AdaptiveThemeMode.light,
        builder: (theme, darkTheme) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Admin Panel',
            theme: theme,
            darkTheme: darkTheme,
            home: MultiProvider(
              providers: [
                ChangeNotifierProvider(
                  create: (context) => MenuAppController(),
                ),
              ],
              child: MainScreen(),
            ),
          );
        });
  }
}
